import backoff
import requests
from bs4 import BeautifulSoup
import re
from datetime import datetime, timedelta
import time
import calendar


class Scrape_OP_GG():
    
    def __init__(self):
        pass

    @backoff.on_exception(backoff.expo,
                      requests.exceptions.RequestException,
                      max_time=60)
    def get_url(self, url):
        return requests.get(url, verify=False)

    def ScrapeSummoner(self, url, page):

        # the url of the webpage of the dataset
        Url = url + str(page)
        print("url: ", Url)

        # connect the page
        Page = self.get_url(Url)

        # get the content of the page
        Soup = BeautifulSoup(Page.content, 'html.parser')

        # get the summoners' name of the website
        try:
            data_dict = {}
            summoners = Soup.find_all("tr", class_="ranking-table__row")
            for summoner in summoners:
                # print(summoner)
                name = summoner.find_all("span")[0].text
                # print(name)
                level = summoner.find_all("td", class_="ranking-table__cell ranking-table__cell--level")[0].text
                level = level.replace('\t', '').replace('\n', '')
                # print(level)
                tier = summoner.find_all("td", class_="ranking-table__cell ranking-table__cell--tier")[0].text
                tier = tier.replace('\t', '').replace('\n', '')
                # print(tier)
                lp = summoner.find_all("td", class_="ranking-table__cell ranking-table__cell--lp")[0].text
                lp = lp.replace('\t', '').replace('\n', '')
                # print(lp)
                # team = summoner.find_all("td", class_="ranking-table__cell ranking-table__cell--team")[0].text
                # team = team.replace('\t', '').replace('\n', '')
                if tier == "Unranked":
                    win_rate = None
                else:
                    win_rate = summoner.find_all("span", class_="winratio__text")[0].text
                feature = {
                           "level": level, 
                           "tier": tier,
                           "lp": lp,
                           "win_rate": win_rate
                           # "team": team
                           }
                # print(feature)
                data_dict[name] = feature

        except Exception as e:
            print("error during getting title: ", e)
            pass

        return data_dict

    def ScrapeMacthTimeAndReturnChurnBoolean(self, url):

        # the url of the webpage of the dataset
        print("url: ", url)

        # connect the page
        Page = self.get_url(url)

        # get the content of the page
        Soup = BeautifulSoup(Page.content, 'html.parser')
        # print(Soup)

        # get the last match time timestamp
        last_match_time = Soup.find_all("div", class_="time-stamp")[0].text
        last_match_timestamp = time.mktime(datetime.strptime(last_match_time, "%Y-%m-%d %H:%M:%S").timetuple())

        # get the current time minus 30 days timestamp
        current_minus_30 = datetime.now() - timedelta(days=30)
        minus_30_timestamp = calendar.timegm(current_minus_30.timetuple())
        # print(minus_30_timestamp)

        # whether churn or not (True for churn, False for retention)
        if minus_30_timestamp > last_match_timestamp:
            return True
        else:
            return False

        '''
        # get the summoners' name of the website
        try:
            data_dict = {}
            summoners = Soup.find_all("tr", class_="ranking-table__row")
            for summoner in summoners:
                # print(summoner)
                name = summoner.find_all("span")[0].text
                # print(name)
                level = summoner.find_all("td", class_="ranking-table__cell ranking-table__cell--level")[0].text
                level = level.replace('\t', '').replace('\n', '')
                tier = summoner.find_all("td", class_="ranking-table__cell ranking-table__cell--tier")[0].text
                tier = tier.replace('\t', '').replace('\n', '')
                # lp = summoner.find_all("td", class_="ranking-table__cell ranking-table__cell--lp")[0].text
                # lp = lp.replace('\t', '').replace('\n', '')
                team = summoner.find_all("td", class_="ranking-table__cell ranking-table__cell--team")[0].text
                team = team.replace('\t', '').replace('\n', '')
                if tier == "Unranked":
                    win_rate = None
                else:
                    win_rate = summoner.find_all("span", class_="winratio__text")[0].text
                feature = {
                           "level": level, 
                           "tier": tier,
                           # "lp": lp
                           "team": team
                           }
                # print(feature)
                data_dict[name] = feature

        except Exception as e:
            print("error during getting title: ", e)
            pass

        return data_dict
        '''

