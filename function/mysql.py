

class MySQLWithPython(): # the function to do SQL command through python

    def __init__(self, config, location):

        self.config = config

        self.location = location

        # whether to choose datatype of SQL
        self.whether_choose_datatype = False

        # whether to SKIP All Error
        self.whether_SKIP_all_error = False

    def ExecuteMysqlCommand(self, sqlcommands):

        config = self.config
        location = self.location

        try:
            # connect to database
            connection = mysql.connector.connect(**config)

            # check whether connected
            if connection.is_connected():
                db_Info = connection.get_server_info()
                print("Connected to MySQL Server version ", db_Info)
                cursor = connection.cursor()
                cursor.execute("select database();")
                record = cursor.fetchone()
                print("You're connected to database: ", record)

            # excute sql code
            cursor = connection.cursor()

            # execute the sql command:
            for sqlcommand in sqlcommands:
                cursor.execute(sqlcommand)
                connection.commit()
                print('Succuessfully execute MySQL Command: ', sqlcommand)

        except mysql.connector.Error as error:
            # if error arouses during the try, print out the error
            print("Failed in MySQL: {}".format(error))
        
        finally:
            if (connection.is_connected()):
                cursor.close()
                connection.close()
                print("MySQL connection is closed")

    def CreateDataBase(self):

        # check whether a database exist

        # create the database if not exist

    def DataTypeDFtoSQL(self, file):

        colnames_datatype = []

        data = pd.read_csv(file)

        colnames = data.columns.values

        # determine the datatype of SQL (net of empty)
        for colname in colnames:

            col_data = data[colname]
            col_data = col_data.dropna()

            if colname == "Unnamed: 0":
                colname = "id"

            colname = self.NameForSQL(colname)

            datatypes = col_data.apply(type)
            datatype = datatypes.describe().top.__name__

            if colname == "id":
                SQL_datatype = "INT"
            elif not self.whether_choose_datatype:
                SQL_datatype = "TEXT"
                print("Specify SQL datatype to TEXT anyway")
            elif datatype == "int":
                SQL_datatype = "INT"
            elif datatype == "str":
                SQL_datatype = "TEXT"
            elif datatype == "float":
                SQL_datatype = "DECIMAL"
            else:
                print("there is another datatype!!!!!!")

            colnames_datatype.append([colname, SQL_datatype])

        return colnames_datatype

    def NameForSQL(self, name):
  
        # replace all marks except for space
        marks = [">", "<", "/", "(", ")"]      
        for mark in marks:
            name = name.replace(mark, "")

        # replace space into _
        name = name.replace(" ", "_")

        return name

    def CommandDealWithEmptyToNULL(self, file):
        """
        function to create command to deal with Empty value and all to create table
        """
        data = pd.read_csv(file)
        colnames = data.columns.values

        # create null command
        nullif_statements = []
        i = 0
        for colname in colnames:
            
            i += 1

            if colname == "Unnamed: 0":
                colname = "id"

            colname = self.NameForSQL(colname)

            nullif_statement = colname + " = NULLIF(@col" + str(i) + ",'')"
            nullif_statements.append(nullif_statement)

        nullif_command = ",".join(nullif_statements)
        nullif_command = " ".join(["SET", nullif_command])

        # create @ statement
        at_col_statements = []
        for i in range(len(colnames)):
            
            i += 1

            at_col_statement = "@col" + str(i)
            at_col_statements.append(at_col_statement)

        at_col_command = ",".join(at_col_statements)
        at_col_command = "(" + at_col_command + ")"

        # full statement
        full_command = " ".join([at_col_command, nullif_command])

        return full_command

    def CommandCreateTable(self, file, table_name, primary=True):
        """
        function for creating elements of table (ex: id INT AUTO)
        """

        colnames_datatype = self.DataTypeDFtoSQL(file)

        elements = []
        for i in range(len(colnames_datatype)):
            colname_datatype = colnames_datatype[i]
            elements.append(" ".join(colname_datatype))

        if primary:
            elements.append("PRIMARY KEY (id)")
        else:
            pass

        table_elements = "(" + ",".join(elements) + ")"

        command = " ".join(["CREATE TABLE", table_name ,table_elements])
        command = command + ";"

        return([command])

    def CommandLoadTable(self, file, table_name):

        # sql command to deal with Empty to NULL
        empty_to_null = self.CommandDealWithEmptyToNULL(file)

        if self.whether_SKIP_all_error:
            command = ("LOAD DATA INFILE '{path}\'" +
                       "SKIP ALL ERRORS" +
                       " INTO TABLE {table_name}" +
                       " FIELDS TERMINATED BY ','" +
                       " ENCLOSED BY '\"'" +
                       " LINES TERMINATED BY '\\n'" +
                       " IGNORE 1 ROWS" +
                       empty_to_null +
                       ";").format(path=file, table_name=table_name)
        else:
            # sql command for loading data
            command = ("LOAD DATA INFILE '{path}\'" +
                       " INTO TABLE {table_name}" +
                       " FIELDS TERMINATED BY ','" +
                       " ENCLOSED BY '\"'" +
                       " LINES TERMINATED BY '\\n'" +
                       " IGNORE 1 ROWS" +
                       empty_to_null +
                       ";").format(path=file, table_name=table_name)
        print(command)

        return ([command])

    def CreateLoadTable(self, title):

        # MySQL command
        print("====================")
        print("loading data: ", title, " into", " database")

        # execute the MySQL command

        # get the file path
        file_path = location + title + ".csv"

        # create table accroding to csv titles
        print("====================")
        print("creating table~~~~~")
        title_for_sql = self.NameForSQL(title) # No special character in table name
        sqlcommands = self.CommandCreateTable(file_path, title_for_sql)
        self.ExecuteMysqlCommand(sqlcommands)

        # load the csv table into DataBase
        print("==================")
        print("loading table~~~~~")
        sqlcommands = self.CommandLoadTable(file_path, title_for_sql)
        print(sqlcommands)
        self.ExecuteMysqlCommand(sqlcommands)
