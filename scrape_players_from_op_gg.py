import os
import json
from function.scrape import Scrape_OP_GG
import time
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()

# prerequiste
region = "na"
euw_url = "https://euw.op.gg/ranking/ladder/page="
kr_url = "https://www.op.gg/ranking/ladder/page="
na_url = "https://na.op.gg/ranking/ladder/page="
euw_last_page = 33323
kr_last_page = 41166
na_last_page = 18559

'''
之後整體的retention rate要按player數量比例去抽，在計算整體的retention rate
'''

mod = 5
euw_mod = euw_last_page // mod
kr_mod = kr_last_page // mod
na_mod = na_last_page // mod

# this file is to scrape the summoner names from op.gg
scrape = Scrape_OP_GG()

for i in range(0, na_mod+1):

    start_time = time.time()

    index = i * mod + 1

    path = './docs/summoners_from_op_gg/' + region + "/" + region + "_" + str(index) + '.json'

    # create the file to store summoner data if not existed
    if not os.path.exists(path):
        empty_dict = {}
        with open(path, 'w') as f:
            json.dump(empty_dict, f)

    with open(path, 'r') as read_file:
        dict_data = json.load(read_file)

    scrape_data = scrape.ScrapeSummoner(url=na_url, page=index)
    for key, item in scrape_data.items():
        dict_data[key] = item

    with open(path, 'w') as write_file:
        json.dump(dict_data, write_file, indent=4)

    print("finish scraping page " + str(index) + " region: " + region)

    print("--- %s seconds ---" % (time.time() - start_time))
    print("===========================")
