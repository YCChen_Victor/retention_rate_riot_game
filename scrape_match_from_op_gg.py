"""
1. 先把name enocde decdoe
2. scrape 最近一次的match time
3. match time 減去 現在的時間
4. > 30天就算churn, label = 1
"""

import os
import json
from function.scrape import Scrape_OP_GG
import time
import random
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()

# prerequiste
region = "kr"

euw_url = "https://euw.op.gg/multi/query="
kr_url = "https://www.op.gg/multi/query="

# read the summoners' name
path = "./docs/summoners_from_op_gg/" + region
for subdir, dirs, files in os.walk(path):

    # each file get two summoners' churn or not
    for file in files:
        try:
            # continue if the file exists
            path = './docs/churn_or_not_from_op_gg/' + region + "/" + file
            if os.path.exists(path):
                print("file scraped " + file)
                continue

            # read the summoners
            filepath = subdir + os.sep + file
            with open(filepath, 'r') as read_file:
                dict_data = json.load(read_file)

            # randomly choose two summoner
            target_dict = {}
            random_summoner_list = random.sample(list(dict_data.keys()), len(dict_data.keys()))

            for summoner in random_summoner_list:
                summoner_data = dict_data[summoner]
                summoner_name = summoner.encode(encoding='utf-8').decode(encoding='utf-8')
                print("scraping " + summoner_name + " from " + file)
                # scrape last match time
                try:
                    if len(list(target_dict.keys())) >= 5:
                        path = './docs/churn_or_not_from_op_gg/' + region + "/" + file
                        # create the file to store summoner data if not existed
                        if not os.path.exists(path):
                            with open(path, 'w') as f:
                                json.dump(target_dict, f)
                        continue
                    url = kr_url + summoner
                    scrape = Scrape_OP_GG()
                    churn_or_not = scrape.ScrapeMacthTimeAndReturnChurnBoolean(url)
                    print("summoner: " + summoner + "; " + str(churn_or_not))
                    print("==============================")
                    summoner_data["churn"] = churn_or_not
                    target_dict[summoner] = summoner_data
                except Exception as e:
                    print("error during getting title: ", e)
                    print("==============================")
                    pass
        except Exception as e:
            print("error during getting title: ", e)
            pass


'''
# this file is to scrape the summoner names from op.gg
scrape = Scrape_OP_GG()

for i in range(0, kr_mod+1):

    start_time = time.time()

    index = i * 50 + 1

    path = './docs/summoners_from_op_gg/' + region + "/" + region + "_" + str(index) + '.json'

    # create the file to store summoner data if not existed
    if not os.path.exists(path):
        empty_dict = {}
        with open(path, 'w') as f:
            json.dump(empty_dict, f)

    with open(path, 'r') as read_file:
        dict_data = json.load(read_file)

    scrape_data = scrape.ScrapeSummonerNameAndLevel(url=kr_url, page=index)
    for key, item in scrape_data.items():
        dict_data[key] = item

    with open(path, 'w') as write_file:
        json.dump(dict_data, write_file, indent=4)

    print("finish scraping page " + str(index) + " region: " + region)

    print("--- %s seconds ---" % (time.time() - start_time))
'''
