import os
import json
import pandas as pd
from lifelines import KaplanMeierFitter
import matplotlib.pyplot as plt

# 這個function還需要再驗證
def level_to_match(basic_experience, average_experience_per_match, level):
    if level in range(1, 75):
        return sum(basic_experience[0: level-1])/average_experience_per_match
    else:
        basic_experience_total = sum(basic_experience[0: 73])
        level_other = level - 74
        mod = level_other % 25
        iteration = level_other // 25
        iteration_experience = list(basic_experience[49: 73])
        experience_needed = basic_experience_total + ((iteration) * sum(iteration_experience)) + sum(iteration_experience[0: mod])
        return experience_needed/average_experience_per_match

# prerequiste
region = "kr"

# read the summoners' name
path = "./docs/churn_or_not_from_op_gg/" + region
for subdir, dirs, files in os.walk(path):

    # each file get two summoners' churn or not
    full_data = {}
    for file in files:
        try:
            # read the summoners
            filepath = subdir + os.sep + file
            with open(filepath, 'r') as read_file:
                dict_data = json.load(read_file)
            for key, item in dict_data.items():
                full_data[key] = item
        except Exception as e:
            print("error during getting title: ", e)
            pass

data = pd.DataFrame.from_dict(full_data, orient='index')

'''
lol_level = pd.read_csv('./docs/level_experience_lol.csv')
for i in range(1, 1000):
    match = level_to_match(lol_level['XP for Next Level'], 200, i)
    print('level: ', i, ' ,matches: ', match)
'''

# Python code to create the above Kaplan Meier curve

## Example Data
lol_level = pd.read_csv('./docs/level_experience_lol.csv')
durations = []
for i in data['level']:
    match = level_to_match(lol_level['XP for Next Level'], 200, int(i))
    durations.append(match)

event_observed = []
for i in data['churn']:
    if i:
        event_observed.append(1)
    else:
        event_observed.append(0)

kmf = KaplanMeierFitter()
kmf.fit(durations, event_observed=event_observed)
kmf.survival_function_
kmf.cumulative_density_
kmf.plot()
plt.show()
