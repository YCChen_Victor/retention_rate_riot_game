import pickle
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np

def get_average_retention_rate(data, weight, duration):
    rate_list = []
    for key, item in data.items():
        x = item["x"]
        y = item["y"]
        target = np.interp(duration, x, y)
        rate_list.append(target)
    result = 0
    for i in range(len(rate_list)):
        result += rate_list[i] * weight[i]
    return result

whole = {
    # all players pages from op gg
    "kr_quantity": 178811,
    "euw_quantity": 307631,
    "jp_quantity": 14403,
    "na_quantity": 250031
}

# start the plot
fig = plt.figure(1, (7,4))
ax = fig.add_subplot(1,1,1)

# load data
with open('./docs/whole_retention_rate.pickle', 'rb') as fp:
    plot_data = pickle.load(fp)

# 假設一個月玩家平均打60場
# get the weight
weight_list = []
for key, item in whole.items():
    weight_list.append(item/sum(whole.values()))

# 這邊是要調整到剛好最長的場次等於一個月打幾場乘以英雄聯盟經過幾個月
game_num_list = []
for i in range(0, 132): #假設11年
    game_num_list.append(i*80) # 一個月打80場

average_rate_list = []
for i in game_num_list:
    average_rate_list.append(get_average_retention_rate(plot_data, weight_list, i))

# plot all retention rate
for key, item in plot_data.items():
    ax.plot(item["x"], item["y"], label=key)

# plot average
ax.plot(game_num_list, average_rate_list, label='average')

'''
# plot 24 months
# print(average_rate_list[0:24])
for i in range(1, 25):
    print(sum(average_rate_list[0:i])/i)
    # print(sum(average_rate_list))
plt.plot(game_num_list[0:24], average_rate_list[0:24], label='average_24_month')
'''

# setting range
plt.gca().set_ylim([0,0.5])

# plot x and y sticks
plt.xticks(game_num_list[1::12])
plt.yticks(np.arange(0, 0.5, 0.05))

# x, y format
plt.xlabel('number of games',fontsize=14)
plt.ylabel('survival rate / retention rate',fontsize=14)
ax.set_yticklabels(['{:.0f}%'.format(x*100) for x in plt.gca().get_yticks()]) 

# y value beside plot
for i,j in zip(game_num_list[1::12], average_rate_list[1::12]):
    ax.annotate(str('{:.2f}%'.format(j*100)),xy=(i, j), weight='bold')

ax.legend(loc='upper right')

plt.show()
