# retention_rate_riot_game

This project is to calculate league of legend retention rate

## Installation

```bash
pip install -r requirements.txt
```

## Usage

### First Approach: (failed)
scrape_leagueid_from_riot.py -> scrape_summonerid_from_riot.py -> scrape_accountid_for_matchlist_from_riot.py -> 

### Second Approach: (success)
scrape_players_from_op_gg.py -> scrape_match_from_op_gg.py -> survival_rate_from_op_gg.py -> presention_retention_rate.py

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)


